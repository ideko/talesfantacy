//
//  StoryVC.m
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "StoryVC.h"

@interface StoryVC ()

@end

@implementation StoryVC
@synthesize currentStory,pageVC,nextPageButton,prevPageButton;

- (id)initWithStory:(Story*)story
{
    self = [super init];
    if (self) {
        self.currentStory = story;
        pageIndex = 0;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor greenColor]];
    
    float w = self.view.frame.size.height;
    float h = self.view.frame.size.width;
    NSLog(@"%f,%f",w,h);
    NSLog(@"storyVC viewDidLoad %@",self.currentStory.storyName);
	// Do any additional setup after loading the view.
    
    //Button page//
    
    self.prevPageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [prevPageButton setTitle:@"<" forState:UIControlStateNormal];
    [prevPageButton setImage:[UIImage imageNamed:@"prevPage.png"] forState:UIControlStateNormal];
    [prevPageButton addTarget:self action:@selector(prevPages:) forControlEvents:UIControlEventTouchUpInside];
    [prevPageButton setFrame:CGRectMake(5, h-10-35, 40, 36.5)];
    [self.view addSubview:prevPageButton];
    
    self.nextPageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextPageButton setTitle:@">" forState:UIControlStateNormal];
    [nextPageButton setImage:[UIImage imageNamed:@"nextPage.png"] forState:UIControlStateNormal];
    [nextPageButton addTarget:self action:@selector(nextPages:) forControlEvents:UIControlEventTouchUpInside];
    [nextPageButton setFrame:CGRectMake(w-15-30, h-10-35, 40, 36.5)];
    [self.view addSubview:nextPageButton];
    
    [self loadPageVCWithPageIndex:pageIndex];
    
    /////////////////
    
}



-(void)loadPageVCWithPageIndex:(int)index{
    NSMutableArray *pagesArray = currentStory.pagesArray;
    
    if (index < [pagesArray count] && index >= 0) {
        if (self.pageVC != nil) {
            if (self.pageVC.view.superview != nil) {
                [self.pageVC.view removeFromSuperview];
            }
            self.pageVC = nil;
        }
        self.pageVC = [[[PageVC alloc] init] autorelease];
        NSMutableDictionary *pageDict = [pagesArray objectAtIndex:index];
        [pageVC setCurrentPageDict:pageDict];

        [self.view insertSubview:pageVC.view atIndex:0];
    }
    
    //////hide button////////
    if ([pagesArray count] <= 1) {
        if (self.nextPageButton.superview != nil) {
            [self.nextPageButton removeFromSuperview];
        }
        if (self.prevPageButton.superview != nil) {
            [self.prevPageButton removeFromSuperview];
        }
        return;
    }
    
    if (pageIndex == [pagesArray count]-1) {
        if (self.nextPageButton.superview != nil) {
            [self.nextPageButton removeFromSuperview];
        }
    } else {
        if (self.nextPageButton.superview == nil) {
            [self.view addSubview:self.nextPageButton];
        }
    }
    
    if (pageIndex == 0) {
        if (self.prevPageButton.superview != nil) {
            [self.prevPageButton removeFromSuperview];
        }
    } else {
        if (self.prevPageButton.superview == nil) {
            [self.view addSubview:self.prevPageButton];
        }
    }
    //////hide button////////

}


-(void)nextPages:(UIButton*)sender{
    NSMutableArray *pagesArray = currentStory.pagesArray;
    if (pageIndex+1 < [pagesArray count]) {
        //not last text in array yet
        pageIndex++;
        [self loadPageVCWithPageIndex:pageIndex];
        
    } else{
        //already last text cannot open next text
    }
}
-(void)prevPages:(UIButton*)sender{
    if (pageIndex > 0) {
        
        //not last text in array yet
        pageIndex--;
        [self loadPageVCWithPageIndex:pageIndex];
        
    } else{
        //already first text cannot open prev text
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
