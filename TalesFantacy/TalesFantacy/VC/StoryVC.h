//
//  StoryVC.h
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "PageVC.h"
@interface StoryVC : UIViewController {
    Story *currentStory;
    int pageIndex;
    PageVC *pageVC;
    UIButton *nextPageButton;
    UIButton *prevPageButton;
 
    
}
@property(nonatomic,retain)Story *currentStory;
@property(nonatomic,retain)PageVC *pageVC;
@property(nonatomic,retain)UIButton *nextPageButton;
@property(nonatomic,retain)UIButton *prevPageButton;

@end
