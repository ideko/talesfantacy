//
//  PageVC.h
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PageVC : UIViewController {
    AppDelegate *appDelegate;
    NSMutableDictionary *currentPageDict;
    UIButton *prevTextButton;
    UIButton *nextTextButton;
    UIButton *textButton;
    int textIndex;
    UIButton *touchDescButton;
}
@property(nonatomic,retain)NSMutableDictionary *currentPageDict;
@property(nonatomic,retain)UIButton *prevTextButton;
@property(nonatomic,retain)UIButton *nextTextButton;
@property(nonatomic,retain)UIButton *textButton;
@property(nonatomic,retain)UIButton *touchDescButton;

@end
