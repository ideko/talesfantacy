//
//  PageVC.m
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "PageVC.h"
#import "AppDelegate.h"
#import "AudioModel.h"


@interface PageVC ()

@end

@implementation PageVC
@synthesize currentPageDict,prevTextButton,nextTextButton,textButton,touchDescButton;

-(id)init{
    self = [super init];
    if (self) {
        appDelegate = [[UIApplication sharedApplication] delegate];
        textIndex = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.view setBackgroundColor:[UIColor grayColor]];
	NSLog(@"PageVC ViewDidLoad currentPageDict %@",currentPageDict);
    float w = self.view.frame.size.height;
    float h = self.view.frame.size.width;
    
    
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
    [backgroundImageView setImage:[UIImage imageNamed:[currentPageDict objectForKey:@"BackgroundImageName"]]];
//    backgroundImageView.animationImages = [NSArray arrayWithObjects:
//                                           [UIImage imageNamed:@"LittleRedBackgroundPage1"],
//                                           [UIImage imageNamed:@"LittleRedBackgroundPage2"],
//                                           nil];
//    backgroundImageView.animationDuration = 0.5;
//    backgroundImageView.animationRepeatCount = 0;
//    [backgroundImageView startAnimating];
    
    [self.view addSubview:backgroundImageView];
    [backgroundImageView release];
    
    
    
    //ButtonText//
    //TODO: change image for prev and next
    self.prevTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [prevTextButton setTitle:@"<" forState:UIControlStateNormal];
    [prevTextButton setImage:[UIImage imageNamed:@"prevButton.png"] forState:UIControlStateNormal];
    [prevTextButton addTarget:self action:@selector(prevText:) forControlEvents:UIControlEventTouchUpInside];
    [prevTextButton setFrame:CGRectMake(5, h-10-287, 25, 25)];
    [self.view addSubview:prevTextButton];
    
    self.nextTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextTextButton setTitle:@">" forState:UIControlStateNormal];
    [nextTextButton setImage:[UIImage imageNamed:@"nextButton.png"] forState:UIControlStateNormal];
    [nextTextButton setFrame:CGRectMake(w-9-20, h-10-287, 25, 25)];
    [nextTextButton addTarget:self action:@selector(nextText:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextTextButton];
    
    self.textButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[textButton setTitle:@"textButton" forState:UIControlStateNormal];
    [textButton setFrame:CGRectMake(w-10-435, h-10-300, 410.5, 51)];
    
    [textButton addTarget:self action:@selector(playAudioForCurrentText) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:textButton];
    
    ////////////////
    
    [self loadTextWithCurrentIndex];
    [self generateTouchableObjects];
    [self generateCoins];
}
-(void)playAudioForCurrentText{
    
    if (appDelegate.audioPlayer.audioPlayer.playing) {
        [appDelegate.audioPlayer stopAudio];
        return;
    }
    [appDelegate.audioPlayer playAudio];
}



-(void)generateTouchableObjects{
    NSMutableArray *touchableArray = [currentPageDict objectForKey:@"TouchableObjectsArray"];
    
    for (int i = 0;  i < [touchableArray count]; i++) {
        NSMutableDictionary *touchableDict = [touchableArray objectAtIndex:i];
        
        UIButton *touchableButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //[touchableButton setBackgroundColor:[UIColor grayColor]];

        [touchableButton setFrame:CGRectMake([[touchableDict objectForKey:@"X"] floatValue], [[touchableDict objectForKey:@"Y"] floatValue], [[touchableDict objectForKey:@"W"] floatValue], [[touchableDict objectForKey:@"H"] floatValue])];
        [self.view addSubview:touchableButton];
        touchableButton.tag = i;
        [touchableButton addTarget:self action:@selector(touch:) forControlEvents:UIControlEventTouchUpInside];
    }

}

-(void)touch:(UIButton*)sender{
    int touchIndex = sender.tag;
    NSMutableArray *touchableArray = [currentPageDict objectForKey:@"TouchableObjectsArray"];
    NSMutableDictionary *touchableDict = [touchableArray objectAtIndex:touchIndex];
    NSLog(@"touched %@",touchableDict);

    if (self.touchDescButton != nil) {
        if (self.touchDescButton.superview != nil) {
            [self.touchDescButton removeFromSuperview];
        }
        self.touchDescButton = nil;
    }
    
    self.touchDescButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [touchDescButton setImage:[UIImage imageNamed:[touchableDict objectForKey:@"TouchImageName"]] forState:UIControlStateNormal];

    [touchDescButton setFrame:CGRectMake([[touchableDict objectForKey:@"X"] floatValue], [[touchableDict objectForKey:@"Y"] floatValue], 133, 64)];
    [touchDescButton setTitle:[touchableDict objectForKey:@"TouchName"] forState:UIControlStateNormal];
    [self.view addSubview:touchDescButton];
    touchDescButton.tag = touchIndex;
    [touchDescButton addTarget:self action:@selector(touchDesc:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)touchDesc:(UIButton*)sender{
    [sender removeFromSuperview];
}

-(void)generateCoins{
    //find coinsArray
    //use for loop to create buttons
    //start from first object in array until the last object using for loop
    
    NSMutableArray *coinsArray = [currentPageDict objectForKey:@"CoinsArray"];
    
    for (int i = 0; i < [coinsArray count];i++) {
        //get coinDict from index i
        //create a button RoundRect
        //set frame using coinDict X,Y
        //add coin to self.view
        
        NSMutableDictionary *coinDict = [coinsArray objectAtIndex:i];
    
        UIButton *coinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [coinButton setImage:[UIImage imageNamed:@"coin.png"] forState:UIControlStateNormal];

        [coinButton setFrame:CGRectMake([[coinDict objectForKey:@"X"] floatValue], [[coinDict objectForKey:@"Y"] floatValue], 32.5, 29)];
        [self.view addSubview:coinButton];
        coinButton.tag = i;
        [coinButton addTarget:self action:@selector(coinPressed:) forControlEvents:UIControlEventTouchUpInside];
        
    }
}

-(void)coinPressed:(UIButton*)sender{
    int coinIndex = sender.tag;
    NSMutableArray *coinsArray = [currentPageDict objectForKey:@"CoinsArray"];
    NSMutableDictionary *coinDict = [coinsArray objectAtIndex:coinIndex];
    NSLog(@"touched coin %@",coinDict);
    [sender removeFromSuperview];
    
    UIButton *coinDescButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [coinDescButton setImage:[UIImage imageNamed:[coinDict objectForKey:@"CoinImageName"]] forState:UIControlStateNormal];
    [coinDescButton setFrame:CGRectMake([[coinDict objectForKey:@"X"] floatValue], [[coinDict objectForKey:@"Y"] floatValue], 133, 64)];
    [coinDescButton setTitle:[coinDict objectForKey:@"CoinName"] forState:UIControlStateNormal];
    [self.view addSubview:coinDescButton];
    coinDescButton.tag = coinIndex;
    [coinDescButton addTarget:self action:@selector(coinDescPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)coinDescPressed:(UIButton*)sender{
    [sender removeFromSuperview];
}

-(void)loadTextWithCurrentIndex{
    NSMutableArray *textsArray = [currentPageDict objectForKey:@"TextsArray"];
    if (textIndex < [textsArray count]) {
        NSMutableDictionary *textDict = [textsArray objectAtIndex:textIndex];
        
        NSLog(@"current text = %@",textDict);
        
        [textButton setImage:[UIImage imageNamed:[textDict objectForKey:@"TextImageName"]] forState:UIControlStateNormal];
        
        [appDelegate.audioPlayer setAudioPlayerWithFileName:[textDict objectForKey:@"TextSoundName"]];
        [appDelegate.audioPlayer playAudio];
    }
    
    //////hide button////////
    if ([textsArray count] <= 1) {
        if (self.nextTextButton.superview != nil) {
            [self.nextTextButton removeFromSuperview];
        }
        if (self.prevTextButton.superview != nil) {
            [self.prevTextButton removeFromSuperview];
        }
        return;
    }
    
    if (textIndex == [textsArray count]-1) {
        if (self.nextTextButton.superview != nil) {
            [self.nextTextButton removeFromSuperview];
        }
    } else {
        if (self.nextTextButton.superview == nil) {
            [self.view addSubview:self.nextTextButton];
        }
    }
    
    if (textIndex == 0) {
        if (self.prevTextButton.superview != nil) {
            [self.prevTextButton removeFromSuperview];
        }
    } else {
        if (self.prevTextButton.superview == nil) {
            [self.view addSubview:self.prevTextButton];
        }
    }
    //////hide button////////
    
}

-(void)nextText:(UIButton*)sender{
    NSMutableArray *textsArray = [currentPageDict objectForKey:@"TextsArray"];
    if (textIndex+1 < [textsArray count]) {
        //not last text in array yet
        textIndex++;
        [self loadTextWithCurrentIndex];
    } else{
        //already last text cannot open next text
    }
}
-(void)prevText:(UIButton*)sender{
    if (textIndex > 0) {
        
        //not last text in array yet
        textIndex--;
        [self loadTextWithCurrentIndex];
    } else{
        //already first text cannot open prev text
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
