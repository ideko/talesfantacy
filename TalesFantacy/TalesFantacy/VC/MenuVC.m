//
//  MenuVC.m
//  TalesFantacy
//
//  Created by ideko on 11/23/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "MenuVC.h"
#import "StoryVC.h"
@interface MenuVC ()

@end

@implementation MenuVC
@synthesize menuLittleRedButton,menuLittlePigsButton,menuGoldilocksButton,menuPinocchioButton,menuCoinsButton,menuGameButton;

-(id)init{
    self = [super init];
    if (self) {
        appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.menuLittleRedButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [menuLittleRedButton setFrame:CGRectMake(10, 10, 20, 20)];
    [menuLittleRedButton addTarget:self action:@selector(littleRedVCOpen) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:menuLittleRedButton];
    
    self.menuLittlePigsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [menuLittlePigsButton setFrame:CGRectMake(30, 30, 40, 40)];
    [menuLittlePigsButton addTarget:self action:@selector(littlePigVCOpen) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:menuLittlePigsButton];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}

-(void)littleRedVCOpen{
    StoryVC *storyVC = [[StoryVC alloc] init];
    [storyVC setCurrentStory:appDelegate.storyModel.littleRed];
    [self.navigationController pushViewController:storyVC animated:YES];
    [storyVC release];
}

-(void)littlePigVCOpen{
    StoryVC *storyVC = [[StoryVC alloc] init];
    [storyVC setCurrentStory:appDelegate.storyModel.littlePigs];
    [self.navigationController pushViewController:storyVC animated:YES];
    [storyVC release];
}

@end
