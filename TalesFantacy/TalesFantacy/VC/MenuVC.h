//
//  MenuVC.h
//  TalesFantacy
//
//  Created by ideko on 11/23/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface MenuVC : UIViewController {
    AppDelegate *appDelegate;
    UIButton *menuLittleRedButton;
    UIButton *menuLittlePigsButton;
    UIButton *menuGoldilocksButton;
    UIButton *menuPinocchioButton;
    UIButton *menuCoinsButton;
    UIButton *menuGameButton;
}
@property(nonatomic,retain) UIButton *menuLittleRedButton;
@property(nonatomic,retain) UIButton *menuLittlePigsButton;
@property(nonatomic,retain) UIButton *menuGoldilocksButton;
@property(nonatomic,retain) UIButton *menuPinocchioButton;
@property(nonatomic,retain) UIButton *menuCoinsButton;
@property(nonatomic,retain) UIButton *menuGameButton;

@end
