//
//  AppDelegate.h
//  TalesFantacy
//
//  Created by ideko on 10/24/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoryModel.h"
#import "AudioModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    StoryModel *storyModel;
    AudioModel *audioPlayer;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) StoryModel *storyModel;
@property (nonatomic,retain) AudioModel *audioPlayer;
@end
