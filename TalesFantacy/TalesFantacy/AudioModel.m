//
//  AudioModel.m
//  TalesFantacy
//
//  Created by ideko on 11/23/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "AudioModel.h"

@implementation AudioModel
@synthesize audioPlayer;

-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)setAudioPlayerWithFileName:(NSString *)fileName{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:[fileName stringByDeletingPathExtension] ofType:[fileName pathExtension]];
    NSData *soundData = [NSData dataWithContentsOfFile:soundPath];
    
    self.audioPlayer = [[[AVAudioPlayer alloc] initWithData:soundData error:nil] autorelease];
}

-(void)playAudio{
    if (self.audioPlayer == nil) {
        return;
    }
    
    [self.audioPlayer play];
}

-(void)stopAudio{
    if (self.audioPlayer == nil) {
        return;
    }
    
    [self.audioPlayer stop];
}

@end
