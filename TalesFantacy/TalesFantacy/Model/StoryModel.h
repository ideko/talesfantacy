//
//  StoryModel.h
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Story.h"
@interface StoryModel : NSObject{
    Story *littleRed;
    Story *littlePigs;
}
@property (nonatomic,retain) Story *littleRed;
@property (nonatomic,retain) Story *littlePigs;


@end
