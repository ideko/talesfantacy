//
//  CoinsModel.h
//  TalesFantacy
//
//  Created by ideko on 11/23/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoinsModel : NSObject{
    NSMutableArray *coinsArray;
}
@property(nonatomic,retain) NSMutableArray *coinsArray;
@end
