//
//  StoryModel.m
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "StoryModel.h"

@implementation StoryModel
@synthesize littleRed,littlePigs;

-(id)init{
    self = [super init];
    if (self) {
        self.littleRed = [[Story alloc] initWithLittleRed];
        self.littlePigs = [[Story alloc] initWithLittlePig];

    }
    return self;
}



@end
