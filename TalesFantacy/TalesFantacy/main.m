//
//  main.m
//  TalesFantacy
//
//  Created by ideko on 10/24/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
