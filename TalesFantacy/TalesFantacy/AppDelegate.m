//
//  AppDelegate.m
//  TalesFantacy
//
//  Created by ideko on 10/24/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "AppDelegate.h"
#import "StoryVC.h"
#import "AudioModel.h"
#import "MenuVC.h"
@implementation AppDelegate

@synthesize audioPlayer,storyModel;

- (void)dealloc
{
    self.storyModel = nil;
    self.audioPlayer = nil;
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    
    self.audioPlayer = [[[AudioModel alloc] init] autorelease];
    self.storyModel = [[[StoryModel alloc] init] autorelease];
//    [audioPlayer setAudioPlayerWithFileName:@"LittleRedBackgroundPage1Text1.m4a"];
//    [audioPlayer playAudio];
    
    MenuVC *menuVC = [[MenuVC alloc] init];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:menuVC];
    navCon.navigationBarHidden = YES;
    self.window.rootViewController = navCon;
    [menuVC release];
    [navCon release];
    
   
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
