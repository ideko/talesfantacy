//
//  AudioModel.h
//  TalesFantacy
//
//  Created by ideko on 11/23/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioModel : NSObject{
    AVAudioPlayer *audioPlayer;
}
@property(nonatomic,retain) AVAudioPlayer *audioPlayer;

-(void)setAudioPlayerWithFileName:(NSString *)fileName;
-(void)playAudio;
-(void)stopAudio;

@end
