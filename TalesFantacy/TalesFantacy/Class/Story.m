//
//  Story.m
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import "Story.h"

@implementation Story
@synthesize pagesArray,storyName;

-(id)initWithLittleRed {
    
    self = [super init];
    if (self) {
        self.storyName = @"หนูน้อยหมวกแดง";
        self.pagesArray = [self generatePagesForLittleRed];
        
        //[self logPageWithIndex:4];
        
        [self logAllPages];
    }
    
    return self;
}

-(id)initWithLittlePig {
    
    self = [super init];
    if (self) {
        self.storyName = @"ลูกหมู 3 ตัว";
        self.pagesArray = [self generatePagesForLittlePig];
        
        //[self logPageWithIndex:4];
        
        [self logAllPages];
    }
    
    return self;
}

-(void)logAllPages{
    for (int i = 0; i < [pagesArray count]; i++) {
        [self logPageWithIndex:i];
    }
}
-(void)logPageWithIndex:(int)index{
    if (index < [pagesArray count]) {
        NSMutableDictionary *pageDict = [pagesArray objectAtIndex:index];
        NSLog(@"page %d = %@",index,pageDict);
    } else {
        NSLog(@"page %d not found",index);
    }
}

-(NSMutableArray *)generatePagesForLittleRed{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *pageDict;
    
    //start page 1///////////////////////////////
    {
    pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage1.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage1Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage1Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage1Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage1Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
          
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
        NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:40] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:140] forKey:@"Y"];
            [coinDict setObject:@"treeImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Tree" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:100] forKey:@"Y"];
            [coinDict setObject:@"hoodImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Hood" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:30] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:205] forKey:@"Y"];
            [coinDict setObject:@"grassImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Grass" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:350] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:103] forKey:@"Y"];
            [coinDict setObject:@"houseImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"House" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
        }
        [pageDict setObject:coinsArray forKey:@"CoinsArray"];
        [coinsArray release];
        
        
        
        NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:15] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"treeImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Tree" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:180] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:80] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"hoodImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Hood" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:30] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:205] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"grassImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Grass" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:340] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"houseImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"House" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
    
        }
        [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
        [touchableObjectsArray release];
        
    [returnArray addObject:pageDict];
    [pageDict release];
        
    }
    //end page 1///////////////////////////////
    
    
    //start page 2///////////////////////////////
    {
        pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage2.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage2Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage2Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage2Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage2Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
        
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
        NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:170] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:250] forKey:@"Y"];
            [coinDict setObject:@"basketButton.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Basket" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:178] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:208] forKey:@"Y"];
            [coinDict setObject:@"cakeButton.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Cake" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:225] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:215] forKey:@"Y"];
            [coinDict setObject:@"milkButton.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Milk" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:142] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:208] forKey:@"Y"];
            [coinDict setObject:@"honeyButton.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Honey" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
        }
        [pageDict setObject:coinsArray forKey:@"CoinsArray"];
        [coinsArray release];
        
        
        
        NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:130] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:250] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"basketButton.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Basket" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:150] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:190] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"cakeButton.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Cake" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:205] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"milkButton.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Milk" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:40] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:210] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"honeyButton.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Honey" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
        
        }
        [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
        [touchableObjectsArray release];
                
        [returnArray addObject:pageDict];
        [pageDict release];
    }
    //end page 2///////////////////////////////
    
    
    //start page 3///////////////////////////////
    {
        pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage3.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage3Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage3Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage3Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage3Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:335] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:105] forKey:@"Y"];
                [coinDict setObject:@"forestButton.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Forest" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:335] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"forestButton.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Forest" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                              
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
    }
    //end page 3///////////////////////////////
    
    
    //start page 4///////////////////////////////
    {
        pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage4.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage4Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage4Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage4Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage4Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
        NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:210] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:180] forKey:@"Y"];
            [coinDict setObject:@"posyImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Posy" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:60] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:250] forKey:@"Y"];
            [coinDict setObject:@"flowerImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Flower" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
         
        }
        [pageDict setObject:coinsArray forKey:@"CoinsArray"];
        [coinsArray release];
        
        
        
        NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:205] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:170] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"posyImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Posy" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:60] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:250] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"flowerImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Flower" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            
        }
        [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
        [touchableObjectsArray release];
        
        [returnArray addObject:pageDict];
        [pageDict release];
    }
    //end page 4///////////////////////////////
    
    
    //start page 5///////////////////////////////
    {
        pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage5.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage5Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage5Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage5Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage5Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
        NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:90] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:210] forKey:@"Y"];
            [coinDict setObject:@"wolfImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Wolf" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
        }
        [pageDict setObject:coinsArray forKey:@"CoinsArray"];
        [coinsArray release];
        
        
        
        NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:40] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:200] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"wolfImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Wolf" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
        }
        [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
        [touchableObjectsArray release];
        
        [returnArray addObject:pageDict];
        [pageDict release];

    }
    //end page 5///////////////////////////////
        
        
        //start page 6///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage6.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage6Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage6Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage6Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage6Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:150] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:208] forKey:@"Y"];
                [coinDict setObject:@"wolfImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Wolf" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:208] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"wolfImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Wolf" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 6///////////////////////////////
        
        
        //start page 7///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage7.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage7Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage7Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage7Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage7Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:160] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:150] forKey:@"Y"];
                [coinDict setObject:@"doorImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Door" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:140] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"doorImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Door" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 7///////////////////////////////
        
        
        //start page 8///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage8.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage8Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage8Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage8Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage8Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:140] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:90] forKey:@"Y"];
                [coinDict setObject:@"wardrobeImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Wardrobe" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
                coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:200] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:220] forKey:@"Y"];
                [coinDict setObject:@"clothImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Cloth" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:130] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:90] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"wardrobeImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Wardrobe" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:200] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:220] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"clothImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Cloth" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 8///////////////////////////////
        
        
        //start page 9///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage9.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage9Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage9Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage9Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage9Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:250] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:170] forKey:@"Y"];
                [coinDict setObject:@"wideopenImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Wideopen" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                            
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:160] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"wideopenImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Wideopen" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 9///////////////////////////////
        
        
        //start page 10///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage10.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage10Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage10Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage10Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage10Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage10Text3.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage10Text3.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:190] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:70] forKey:@"Y"];
                [coinDict setObject:@"earsImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Ears" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
                coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:210] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:100] forKey:@"Y"];
                [coinDict setObject:@"eyesImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Eyes" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:150] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:70] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"earsImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Ears" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:210] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:100] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"eyesImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Eyes" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 10///////////////////////////////
        
        
        //start page 11///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage11.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage11Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage11Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage11Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage11Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                            
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:270] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:177] forKey:@"Y"];
                [coinDict setObject:@"mouthImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Mouth" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
                coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:190] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:115] forKey:@"Y"];
                [coinDict setObject:@"bedImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Bed" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
                coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:140] forKey:@"Y"];
                [coinDict setObject:@"noseImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Nose" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:177] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"mouthImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Mouth" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:150] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:90] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"bedImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Bed" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:200] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:130] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"noseImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Nose" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 11///////////////////////////////
    
        
        //start page 12///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage12.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage12Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage12Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage12Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage12Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:160] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:232] forKey:@"Y"];
                [coinDict setObject:@"hunterButton.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Hunter" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:140] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:232] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"hunterButton.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Hunter" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 12///////////////////////////////
        
        
        //start page 13///////////////////////////////
        {
            pageDict = [[NSMutableDictionary alloc] init];
            [pageDict setObject:@"LittleRedBackgroundPage13.png" forKey:@"BackgroundImageName"];
            NSMutableArray *textsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage13Text1.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage13Text1.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                textDict = [[NSMutableDictionary alloc] init];
                [textDict setObject:@"LittleRedBackgroundPage13Text2.png" forKey:@"TextImageName"];
                [textDict setObject:@"LittleRedBackgroundPage13Text2.m4a" forKey:@"TextSoundName"];
                [textsArray addObject:textDict];
                [textDict release];
                
                
            }
            [pageDict setObject:textsArray forKey:@"TextsArray"];
            [textsArray release];
            
            NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:140] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:150] forKey:@"Y"];
                [coinDict setObject:@"glassesImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Glasses" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
                coinDict = [[NSMutableDictionary alloc] init];
                [coinDict setObject:[NSNumber numberWithInt:160] forKey:@"X"];
                [coinDict setObject:[NSNumber numberWithInt:210] forKey:@"Y"];
                [coinDict setObject:@"grandmotherImage.png" forKey:@"CoinImageName"];
                [coinDict setObject:@"Grandmother" forKey:@"CoinName"];
                [coinsArray addObject:coinDict];
                [coinDict release];
                
            }
            [pageDict setObject:coinsArray forKey:@"CoinsArray"];
            [coinsArray release];
            
            
            
            NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
            {
                NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:140] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:150] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"glassesImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Glasses" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
                touchDict = [[NSMutableDictionary alloc] init];
                [touchDict setObject:[NSNumber numberWithInt:150] forKey:@"X"];
                [touchDict setObject:[NSNumber numberWithInt:210] forKey:@"Y"];
                [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
                [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
                [touchDict setObject:@"grandmotherImage.png" forKey:@"TouchImageName"];
                [touchDict setObject:@"Grandmother" forKey:@"TouchName"];
                [touchableObjectsArray addObject:touchDict];
                [touchDict release];
                
            }
            [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
            [touchableObjectsArray release];
            
            [returnArray addObject:pageDict];
            [pageDict release];
            
        }
        //end page 13///////////////////////////////
    
    return [returnArray autorelease];
    }


-(NSMutableArray *)generatePagesForLittlePig{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *pageDict;
    
    //start page 1///////////////////////////////
    {
        pageDict = [[NSMutableDictionary alloc] init];
        [pageDict setObject:@"LittleRedBackgroundPage1.png" forKey:@"BackgroundImageName"];
        NSMutableArray *textsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage1Text1.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage1Text1.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
            textDict = [[NSMutableDictionary alloc] init];
            [textDict setObject:@"LittleRedBackgroundPage1Text2.png" forKey:@"TextImageName"];
            [textDict setObject:@"LittleRedBackgroundPage1Text2.m4a" forKey:@"TextSoundName"];
            [textsArray addObject:textDict];
            [textDict release];
            
        }
        [pageDict setObject:textsArray forKey:@"TextsArray"];
        [textsArray release];
        
        NSMutableArray *coinsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:40] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:140] forKey:@"Y"];
            [coinDict setObject:@"treeImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Tree" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:230] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:100] forKey:@"Y"];
            [coinDict setObject:@"hoodImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Hood" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:30] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:205] forKey:@"Y"];
            [coinDict setObject:@"grassImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"Grass" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
            
            coinDict = [[NSMutableDictionary alloc] init];
            [coinDict setObject:[NSNumber numberWithInt:350] forKey:@"X"];
            [coinDict setObject:[NSNumber numberWithInt:103] forKey:@"Y"];
            [coinDict setObject:@"houseImage.png" forKey:@"CoinImageName"];
            [coinDict setObject:@"House" forKey:@"CoinName"];
            [coinsArray addObject:coinDict];
            [coinDict release];
        }
        [pageDict setObject:coinsArray forKey:@"CoinsArray"];
        [coinsArray release];
        
        
        
        NSMutableArray *touchableObjectsArray = [[NSMutableArray alloc] init];
        {
            NSMutableDictionary *touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:15] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"treeImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Tree" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:180] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:80] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"hoodImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Hood" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:30] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:205] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"grassImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"Grass" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
            touchDict = [[NSMutableDictionary alloc] init];
            [touchDict setObject:[NSNumber numberWithInt:340] forKey:@"X"];
            [touchDict setObject:[NSNumber numberWithInt:120] forKey:@"Y"];
            [touchDict setObject:[NSNumber numberWithInt:133] forKey:@"W"];
            [touchDict setObject:[NSNumber numberWithInt:64] forKey:@"H"];
            [touchDict setObject:@"houseImage.png" forKey:@"TouchImageName"];
            [touchDict setObject:@"House" forKey:@"TouchName"];
            [touchableObjectsArray addObject:touchDict];
            [touchDict release];
            
        }
        [pageDict setObject:touchableObjectsArray forKey:@"TouchableObjectsArray"];
        [touchableObjectsArray release];
        
        [returnArray addObject:pageDict];
        [pageDict release];
        
    }
    return [returnArray autorelease];
}


@end
