//
//  Story.h
//  TalesFantacy
//
//  Created by ideko on 11/18/55 BE.
//  Copyright (c) 2555 ideko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Story : NSObject{
    NSMutableArray *pagesArray;
    NSString *storyName;
    
}
@property (nonatomic,retain)NSMutableArray *pagesArray;
@property (nonatomic,retain)NSString *storyName;

-(id)initWithLittleRed;
-(id)initWithLittlePig;

@end
